package com.example.helloworldspring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@WebMvcTest(HelloController.class)
public class HelloControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void sayHello_noArgs_returnsHelloWorld() throws Exception {
        //setup
        //execute
        mockMvc.perform(MockMvcRequestBuilders.get("/hello"))
                //assert
                //.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello from Spring"));
    }

    @Test
    public void sayHello_withName_saysHelloNameFromSpring() throws Exception {
        //assert
        mockMvc.perform(MockMvcRequestBuilders.get("/hello?name=Monica"))
                //.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Hello Monica from Spring"));

    }
}
