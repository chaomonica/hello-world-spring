package com.example.helloworldspring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(CalculateController.class)
public class CalculateControllerTest {

    @Autowired
    private MockMvc mockMvp;

    @Test
    public void calculate_addition_returnsResult() throws Exception {
        mockMvp.perform(MockMvcRequestBuilders.get("/calculate?math=1+1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));
    }

    @Test
    public void calculate_subtraction_returnsResult() throws Exception {
        mockMvp.perform(MockMvcRequestBuilders.get("/calculate?math=1-1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("0"));
    }

    @Test
    public void calculate_multiplication_returnsResult() throws Exception {
        mockMvp.perform(MockMvcRequestBuilders.get("/calculate?math=1*1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1"));
    }

    @Test
    public void calculate_division_returnsResult() throws Exception {
        mockMvp.perform(MockMvcRequestBuilders.get("/calculate?math=4/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));
    }
}
