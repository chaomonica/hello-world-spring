package com.example.helloworldspring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest
public class VowelCounterTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void vowelCounter_String_returnsNumberOfVowels() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowelCounter?word=hello"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));
    }
}
