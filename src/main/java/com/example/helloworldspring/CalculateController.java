package com.example.helloworldspring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {

    @GetMapping("/calculate")
    public String calculate(@RequestParam String math){
        String[] mathString;
        if (math.contains("+"))  {
            mathString = math.split("\\+");
            return "" + (Integer.parseInt(mathString[0]) + Integer.parseInt(mathString[1]));
      } else if (math.contains("-"))  {
            mathString = math.split("-");
            return "" + (Integer.parseInt(mathString[0]) - Integer.parseInt(mathString[1]));
        }
        else if (math.contains("*"))  {
            mathString = math.split("\\*");
            return "" + (Integer.parseInt(mathString[0]) * Integer.parseInt(mathString[1]));
        }       else if (math.contains("/"))  {
            mathString = math.split("/");
            return "" + (Integer.parseInt(mathString[0]) / Integer.parseInt(mathString[1]));
        }
        return "Please submit calculation query in the form of: numA operator numB";

    }

}
