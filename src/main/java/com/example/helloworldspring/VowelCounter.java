package com.example.helloworldspring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VowelCounter {
    @GetMapping("/vowelCounter")
    public String countVowels(@RequestParam String word) {
        int counter = 0;

        for (int i = 0; i < word.length(); i++){
            String temp = Character.toString(word.charAt(i));
            if (Character.toString(word.charAt(i)).matches("[aeiouAEIOU]")) {
                counter++;
            }
        }
        return "" + counter;
    }
}
